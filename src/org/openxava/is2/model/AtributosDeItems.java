package org.openxava.is2.model;

import javax.persistence.*;

@Entity
public class AtributosDeItems extends Identifiable{
	@ManyToOne(
			fetch=FetchType.LAZY,
			optional=false) 
			@JoinColumn(name="idAtributoPorItem",insertable = false, updatable = false)
	private AtributosPorItem atributoPorItem;

	
	@ManyToOne(
			fetch=FetchType.LAZY,
			optional=false) 
			@JoinColumn(name="id_item")
	private Item item;
	

	@Column(length=50)
	private String valor;


	public AtributosPorItem getAtributoPorItem() {
		return atributoPorItem;
	}

	public void setAtributoPorItem(AtributosPorItem atributoPorItem) {
		this.atributoPorItem = atributoPorItem;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}


	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
}
