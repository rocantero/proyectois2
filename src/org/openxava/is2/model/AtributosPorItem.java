package org.openxava.is2.model;

import java.util.List;

import javax.persistence.*;

import org.openxava.annotations.*;
@Entity
public class AtributosPorItem extends Identifiable {

	@OneToMany(
			fetch=FetchType.LAZY,
			mappedBy="atributoPorItem")
	private List<AtributosDeItems> atributosDeItems;
	
	public List<AtributosDeItems> getAtributosDeItems() {
		return atributosDeItems;
	}

	public void setAtributosDeItems(List<AtributosDeItems> atributosDeItems) {
		this.atributosDeItems = atributosDeItems;
	}

	public String getValorPorDefecto() {
		return valorPorDefecto;
	}

	public void setValorPorDefecto(String valorPorDefecto) {
		this.valorPorDefecto = valorPorDefecto;
	}

	@Column(length=30)
	private String nombre;
	
	@Column(length=30)
	private String tipo;
	
	@Column(length=50)
	private String valorPorDefecto;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getValor_por_defecto() {
		return valorPorDefecto;
	}

	public void setValor_por_defecto(String valorPorDefecto) {
		this.valorPorDefecto = valorPorDefecto;
	}
	
	
}
