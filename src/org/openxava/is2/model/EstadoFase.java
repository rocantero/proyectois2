package org.openxava.is2.model;

public enum EstadoFase {
	Abierta,
	NoIniciada,
	Finalizada
}