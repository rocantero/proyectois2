package org.openxava.is2.model;

public enum EstadoItem {
	Desaprobado,
	Aprobado,
	Bloqueado
}
