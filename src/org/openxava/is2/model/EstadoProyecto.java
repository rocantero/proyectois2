package org.openxava.is2.model;

public enum EstadoProyecto {
	Activo,
	Anulado,
	Pendiente,
	Finalizado
}
