package org.openxava.is2.model;

import java.util.*;

import javax.persistence.*;

import org.hibernate.validator.*;
import org.openxava.annotations.*;
import org.openxava.view.View;

@Entity


public class Fase extends Identifiable {
	@ManyToOne(
			fetch=FetchType.LAZY,
			optional=false) 
			@JoinColumn(name="id_item_actual")
	private ItemActual itemActual;

	@Column @Required
	private int posicionFase;

	@Column @Required
	private String nombre;
	
	@Column 
	private String descripcion;
	
	@Column @Required
	private int nroItems;
	
	@Column @Required
	private int nroLb;
	
	
	@Required
	@Column(length=15) 
	@Enumerated(EnumType.STRING)
	private EstadoFase estado = EstadoFase.Abierta;
	
	@ManyToOne(
			fetch=FetchType.LAZY,
			optional=false) 
			@JoinColumn(name="idproyecto")
	private Proyecto proyecto;
	
	@ManyToMany
	@JoinTable(name = "fase_item", joinColumns = { @JoinColumn(name = "fase_id", referencedColumnName = "oid") }, inverseJoinColumns = { @JoinColumn(name = "item_id", referencedColumnName = "oid") })
	private List<Item> items;
	
	public Proyecto getProyecto() {
		return proyecto;
	}

	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}

	public int getposicionFase() {
		return posicionFase;
	}

	public void setposicionFase(int posicionFase) {
		this.posicionFase = posicionFase;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getnroItems() {
		return nroItems;
	}

	public void setnroItems(int nroItems) {
		this.nroItems = nroItems;
	}

	public int getnroLb() {
		return nroLb;
	}

	public void setnroLb(int nroLb) {
		this.nroLb = nroLb;
	}

	public ItemActual getItemActual() {
		return itemActual;
	}

	public void setItemActual(ItemActual itemActual) {
		this.itemActual = itemActual;
	}

	public int getPosicionFase() {
		return posicionFase;
	}

	public void setPosicionFase(int posicionFase) {
		this.posicionFase = posicionFase;
	}

	public int getNroItems() {
		return nroItems;
	}

	public void setNroItems(int nroItems) {
		this.nroItems = nroItems;
	}

	public int getNroLb() {
		return nroLb;
	}

	public void setNroLb(int nroLb) {
		this.nroLb = nroLb;
	}

	public EstadoFase getEstado() {
		return estado;
	}

	public void setEstado(EstadoFase estado) {
		this.estado = estado;
	}
	
	public void setParent(Proyecto parent) {
		this.proyecto = parent;
	}

	public Proyecto getParent() {
		return proyecto;
	}
	
	private void validateFase(String operacion) throws Exception {
		if(this.proyecto.getEstado().equals("Finalizado")){
			throw new InvalidStateException(
					new InvalidValue[] {
							new InvalidValue(
									"Este proyecto ya esta FINALIZADO",
									getClass(), "esta",
									operacion, this)
					}
			);
		}
	}
	
	@PrePersist
	private void onPersist()throws Exception{
		this.validateFase("INSERTAR");
	}
	
	@PreUpdate
	public void onUpdate()throws Exception{
		this.validateFase("MODIFICAR");
		//if("FINALIZADA".equals(estadoFase.getNombre())){
		if(this.estado.equals("Finalizada")){	
			if(items != null && !items.isEmpty()){
				boolean v = true;
				for (Item item : items) {
					if(item.getEstado() != null 
							&& !"Aprobado".equals(item.getEstado())){
						
						v = false;
						break;
					}
				}
				
				if (!v) {
					throw new InvalidStateException(
							new InvalidValue[] {
									new InvalidValue(
											"Todos los items de la fase deben estar en estado Aprobado",
											getClass(), "Estado",
											"Finalizada", this)
							}
					);
				}
				
			}
		}
	}
	
	@PreRemove
	private void onRemove() throws Exception{
		this.validateFase("ELIMINAR");
	}
	
//	private boolean check(Fase f){
//		boolean b=false;
//		for(int i=0;i<=f.items.size();i++){
//			Item it=f.items.get(i);
//			if(it.getEstado()=="Aprobado"){
//				b=true;
//			}
//		}
//		return b;
//	}
	
	
}
