package org.openxava.is2.model;

import javax.persistence.*;
import org.openxava.annotations.*;
@Entity
public class HistorialItems extends Identifiable {
	
	@ManyToOne(
			fetch=FetchType.LAZY,
			optional=false) 
			@JoinColumn(name="id_item")
	private Item item;

	@Column
	private String tipoModificacion;
	
	@Column @Stereotype("FECHAHORA")
	private String fechaModificacion;

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public String getTipoModificacion() {
		return tipoModificacion;
	}

	public void setTipoModificacion(String tipoModificacion) {
		this.tipoModificacion = tipoModificacion;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}


	
	
}
