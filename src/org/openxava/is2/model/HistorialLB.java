package org.openxava.is2.model;

import javax.persistence.*;
import org.openxava.annotations.*;

@Entity
public class HistorialLB extends Identifiable{
	@ManyToOne(
		fetch=FetchType.LAZY,
		optional=true) 
	@JoinColumn(name="lb")
	private LB lb;
	@Column
	private String tipoModificacion;
	
	@Column @Stereotype("FECHAHORA")
	private String fechaModificacion;

	public LB getLb() {
		return lb;
	}

	public void setLb(LB lb) {
		this.lb = lb;
	}

	public String gettipoModificacion() {
		return tipoModificacion;
	}

	public void settipoModificacion(String tipoModificacion) {
		this.tipoModificacion = tipoModificacion;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	
}
