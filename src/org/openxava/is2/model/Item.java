package org.openxava.is2.model;

import java.util.*;

import javax.persistence.*;

import org.hibernate.validator.*;
import org.openxava.annotations.*;

@Entity
public class Item extends Identifiable{

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "item")
	private List<AtributosDeItems> atributos;
	
	@ManyToOne(fetch=FetchType.LAZY, optional = false)
	private Fase fase;
	 
	@Column
	private int version;
	
	@Column
	private int complejidad;
	
	@Column
	private int prioridad;
	
	@Column
	private String estado;

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getComplejidad() {
		return complejidad;
	}

	public void setComplejidad(int complejidad) {
		this.complejidad = complejidad;
	}

	public int getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	@ManyToMany(mappedBy="Item")
	private List<Usuario> Usuarios;

	public List<AtributosDeItems> getAtributos() {
		return atributos;
	}

	public void setAtributos(List<AtributosDeItems> atributos) {
		this.atributos = atributos;
	}
	
	public Fase getParent() {
		return fase;
	}

	public void setParent(Fase parent) {
		this.fase = parent;
	}
	
	private void validateItem(String operacion) throws Exception {
		if("Finalizada".equals(fase.getEstado())){
			throw new InvalidStateException(
					new InvalidValue[] {
							new InvalidValue(
									"Item pertenece a una fase Finalizada",
									getClass(), "este",
									operacion, this)
					}
			);
		}
	}

	@PreRemove
	public void onRemove()throws Exception{
		this.validateItem("ELIMINAR");
	}
	
	@PrePersist
	public void onPersist()throws Exception{
		this.validateItem("INSERTAR");
	}
	/*
	@PreUpdate
	public void onUpdate()throws Exception{
		this.validateItem("MODIFICAR");
		if("Finalizado".equals(estado)){
			if(atributos != null && !atributos.isEmpty()){
				boolean v = true;
				for (AtributosDeItems atr : atributos) {
					if(atr.getEstado() != null 
							&& !"FINALIZADO".equals(atr.getEstadoAtributo().getNombre())){
						
						v = false;
						break;
					}
				}
				
				if (!v) {
					throw new InvalidStateException(
							new InvalidValue[] {
									new InvalidValue(
											"Todos los atributos del item deben estar en estado FINALIZADO",
											getClass(), "Estado",
											"FINALIZADO", this)
							}
					);
				}
				
			}
		}
	}
	*/
}
