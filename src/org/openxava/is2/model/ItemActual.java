package org.openxava.is2.model;

import javax.persistence.*;

import org.openxava.annotations.*;

@Entity
public class ItemActual extends Identifiable{

	@Column
	@Required
	private int nroPorTipo;
	
	@Column
	@Required
	private int numero;
	
	@Column
	private boolean eliminado;
	
	@ManyToOne(
			fetch=FetchType.LAZY,
			optional=false) 
			@JoinColumn(name="item")
	private Item item;

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public boolean isEliminado() {
		return eliminado;
	}

	public int getNroPorTipo() {
		return nroPorTipo;
	}

	public void setNroPorTipo(int nroPorTipo) {
		this.nroPorTipo = nroPorTipo;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}
	
}
