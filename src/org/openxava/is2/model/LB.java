package org.openxava.is2.model;

import java.util.List;

import javax.persistence.*;

import org.openxava.annotations.*;

@Entity
public class LB extends Identifiable{
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "lb")
	@ListAction("OneToMany.new")
    private List<HistorialLB> historialesLB;
	
	
	@Column
	private int numero;
	
	@Column
	private String estado;
	public List<HistorialLB> getHistorialesLB() {
		return historialesLB;
	}
	public void setHistorialesLB(List<HistorialLB> historialesLB) {
		this.historialesLB = historialesLB;
	}
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
}
