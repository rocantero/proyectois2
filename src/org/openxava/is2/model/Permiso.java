package org.openxava.is2.model;

import java.util.List;

import javax.persistence.*;

@Entity

public class Permiso extends Identifiable{
	@ManyToMany(mappedBy="permisos")
	private List<Rol> rol;
	

	public List<Rol> getRol() {
		return rol;
	}

	public void setRol(List<Rol> rol) {
		this.rol = rol;
	}

	@Column 
	private String nombre;
	
	@Column
	private String codigo;


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
}
