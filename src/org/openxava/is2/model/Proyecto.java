package org.openxava.is2.model;
import java.util.*;

import javax.persistence.*;

import org.openxava.annotations.*;


@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "nombre" }))
public class Proyecto extends Identifiable {

	@Column
	@Required
	private String nombre;

	@Column
	@Required
	private String descripcion;

	@Column(name = "fechaCreacion")
	@Stereotype("FECHAHORA")
	private Date fechaCreacion;

	@Column(name = "complejidadTotal")
	private int complejidadTotal;

	@Column
	@Enumerated(EnumType.STRING)
	@Required
	private EstadoProyecto estado = EstadoProyecto.Pendiente;

	@Column(name = "nroFases")
	@Required
	private int nroFases;

	public List<Fase> getFases() {
		return fases;
	}

	public void setFases(List<Fase> fases) {
		this.fases = fases;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "proyecto")
	private List<Fase> fases;

	@Required
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "lider")
	private Usuario lider;

	public void setUsuarios(List<Usuario> usuarios) throws Exception {
		this.usuarios = usuarios;
	}

	@ManyToMany
	@JoinTable(name = "proyecto_usuario", joinColumns = { @JoinColumn(name = "proyecto_id", referencedColumnName = "oid") }, inverseJoinColumns = { @JoinColumn(name = "usuario_id", referencedColumnName = "oid") })
	private List<Usuario> usuarios;

	@ManyToMany
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public int getComplejidadTotal() {
		return complejidadTotal;
	}

	public void setComplejidadTotal(int complejidadTotal) {
		this.complejidadTotal = complejidadTotal;
	}

	public EstadoProyecto getEstado() {
		return estado;
	}

	public void setEstado(EstadoProyecto estado) {
		this.estado = estado;
	}

	public int getNroFases() {
		return nroFases;
	}

	public void setNroFases(int nroFases) {
		this.nroFases = nroFases;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuario(Usuario usuario) {
		usuarios.add(usuario);
	}

	public Usuario getLider() {
		return lider;
	}

	public void setLider(Usuario usuario) {
		this.lider = usuario;
	}

	// @PrePersist, @PostPersist, @PreRemove, @PostRemove, @PreUpdate,
	// @PostUpdate y @PostLoad.
	@PrePersist
	private void alCrear() {
		setEstado(EstadoProyecto.Pendiente);
		setFechaCreacion(new Date());
	}
	@PreRemove
	private void alBorrar() throws Exception{
		throw new Exception("No se puede borrar el proyecto");
	}
	@PreUpdate
	private void alActualizar() throws Exception{
		Proyecto p = new Proyecto();
		Usuario u = new Usuario();
		if(getEstado() == estado.Finalizado){
			throw new Exception("El proyecto ya fue finalizado");
		}
	}
	
}
