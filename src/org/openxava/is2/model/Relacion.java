package org.openxava.is2.model;

import javax.persistence.*;

@Entity
public class Relacion extends Identifiable{

	@Column
	private String tipo;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
