package org.openxava.is2.model;

import javax.persistence.*;

@Entity
public class RelacionPorItem extends Identifiable{
	@ManyToOne(
			fetch=FetchType.LAZY,
			optional=false) 
			@JoinColumn(name="relacion")
	private Relacion relacion;

	public Relacion getRelacion() {
		return relacion;
	}

	public void setRelacion(Relacion relacion) {
		this.relacion = relacion;
	}

	public void setRevisado(boolean revisado) {
		this.revisado = revisado;
	}

	@Column
	private boolean revisado;

	public boolean isRevisado() {
		return revisado;
	}

	
}
