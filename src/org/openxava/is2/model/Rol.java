package org.openxava.is2.model;

import java.util.*;

import javax.persistence.*;

import org.openxava.annotations.*;

@Entity
public class Rol extends Identifiable {		
	@ManyToMany
	@JoinTable(
		name="rol_permiso",
		joinColumns={@JoinColumn(name="rol_id", referencedColumnName="oid")},
		inverseJoinColumns={@JoinColumn(name="permiso_id", referencedColumnName="oid")})
	private List<Permiso> permisos;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rol")
	private List<Usuario> usuarios;
	
	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	@Column(length = 200)
	private String descripcion;

	@Column(length = 30)
	private String nombre;

//	@ManyToMany
//	@ListAction("ManyToMany.new")
//	private List<Permiso> Permisos;

	public List<Permiso> getPermisos() {
		return permisos;
	}

	public void setPermisos(List<Permiso> permisos) {
		this.permisos = permisos;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return nombre;
		
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
