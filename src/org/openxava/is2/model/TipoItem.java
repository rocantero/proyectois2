package org.openxava.is2.model;

import javax.persistence.*;
import org.openxava.annotations.*;

@Entity
public class TipoItem extends Identifiable{

	@Column
	@Required
	private String codigo;
	
	@Column
	@Required
	private String descripcion;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
