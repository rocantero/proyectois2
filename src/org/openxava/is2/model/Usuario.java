package org.openxava.is2.model;

import java.util.List;

import javax.persistence.*;

import org.openxava.annotations.*;

@Entity


public class Usuario extends Identifiable {
	
	@ManyToMany
	@JoinTable(
		name="usuario_item",
		joinColumns={@JoinColumn(name="usuario_id", referencedColumnName="oid")},
		inverseJoinColumns={@JoinColumn(name="item_id", referencedColumnName="oid")})
	private List<Item> Item;
	
	@Required
	@ManyToOne(optional=false, fetch=FetchType.LAZY)
	@JoinColumn(name="rol")
	private Rol rol;

	@Column @Required
	private String nombre;
	
	@Column
	private String apellido;
	
	@Column @Required
	private String username;
	
	@Column @Required @Stereotype("PASSWORD")
	private String password;
	
	@Column
	@Stereotype("TELEPHONE")
	private String telefono;

	public List<Item> getItem() {
		return Item;
	}

	public void setItem(List<Item> item) {
		Item = item;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public EstadoUsuario getEstado() {
		return estado;
	}

	public void setEstado(EstadoUsuario estado) {
		this.estado = estado;
	}

	@Column  
	private String cedula;
	
	@ManyToMany(mappedBy="usuarios")
	private List<Proyecto> proyectos;
	
	@Column @Stereotype("EMAIL")
	private String email;
	
	@Column 
	private EstadoUsuario estado;
	public enum EstadoUsuario {ACTIVO, INACTIVO}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public List<Proyecto> getProyectos(){
		return proyectos;
	}
	public void setProyecto(Proyecto proyecto){
		this.proyectos.add(proyecto);
	}

	public void setProyectos(List<Proyecto> proyectos) {
		this.proyectos = proyectos;
	}
}
